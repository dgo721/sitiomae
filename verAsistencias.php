<?php
	include "conexion.php";
	include "accesaAdmin.php";
	
	$sql="SELECT 
					*
				FROM
					usuario u left join carrera c on  u.cve_carrera = c.cve_carrera
				where
					 u.tipo = 'i' ";
	$buscarPor="";
	if(isset( $_GET['buscarPor'])){
		$buscarPor=$_GET['buscarPor'];
		$buscarContenga=$_GET['buscarContenga'];
		switch($buscarPor){
			case "matricula":
				$sql.="and login like '%$buscarContenga%'";
				break;
			case "carrera":
				$sql.="and siglas like '%$buscarContenga%' or descripcion like '%$buscarContenga%'";
				break;
			case "nombre":
				$sql.="and nombre like '%$buscarContenga%'";
				break;
			case "aPaterno":
				$sql.="and aPaterno like '%$buscarContenga%'";
				break;
			case "aMaterno":
				$sql.="and aMaterno like '%$buscarContenga%' ";
				break;
		}
	}
	$sql.="order by ISNULL(nombre), nombre ASC";
?>


<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Andres Cavazos">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">


		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">

		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="indexAdmin.php">Bienvenido a MAES</a>
			</div>
			<div>
			  <ul class="nav navbar-nav" style="float:right">
				<li class="active"><a href="verAsistencias.php">Asistencias</a></li>
				<li><a href="altaInstructor.php">Instructores</a></li>
				<li><a href="altaMateria.php">Materias</a></li>
				<li><a href="altaCarrera.php">Carreras</a></li>
				<li><a href="borrarBD.php">Borrar base de datos</a></li>
				<li><a href="ayuda.php">Ayuda</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
			  </ul>
			</div>
		  </div>
		</div>
		
		<figure style="float:left;margin-top:-20px;position:static">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>
		
		<center>
			<form class="form-inline" role="form" method="get" action='verAsistencias.php'>
				<table>
					<tr>
						<td>
							<h3>Buscar por:&nbsp;</h3>
						</td>
						<td>	
							<select class="form-control" name="buscarPor">
								<option value="nombre" <?php if($buscarPor=="nombre")echo "selected";?>>Nombre</option>
								<option value="aPaterno" <?php if($buscarPor=="aPaterno")echo "selected";?>>Apellido Paterno</option>
								<option value="aMaterno" <?php if($buscarPor=="aMaterno")echo "selected";?>>Apellido Materno</option>
								<option value="carrera" <?php if($buscarPor=="carrera")echo "selected";?>>Carrera</option>
								<option value="matricula" <?php if($buscarPor=="matricula")echo "selected";?>>Matricula</option>
							</select>						
						</td>
						<td>
							&nbsp;&nbsp;
						</td>
						<td>
							<h3>Que contenga:&nbsp;</h3>
						</td>
						<td>
							<input type="text" class="form-control" name="buscarContenga" <?php if(isset($buscarContenga)) echo "value=\"$buscarContenga\""?>>
						</td>
						<td>
							&nbsp;<button type="submit" class="btn btn-default">Buscar</button>
						</td>
					</tr>
				</table>
			</form>
		</center>

		<br/>

		<div class="container">
			<table class="table table-hover" style="background-color:white;">
				<thead>
					<tr>
						<th>Matricula</th>
						<th>Nombre</th>
						<th>Carrera</th>
					</tr>
				</thead>
				<?php
					$result = mysqli_query($con,$sql);

					if ($result!=""){
						while($row = mysqli_fetch_array($result)){
	
							echo "<tr>
										<td><a href=\"horariosAdmin.php?login=".$row['login']."\">".$row['login']."</a></td>
										<td>".$row['nombre']." ".$row['aPaterno']." ".$row['aMaterno']."</td>
										<td>".$row['siglas']."</td>
									</tr>";
						}
					} else {
						echo "<tr>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
									</tr>";
					}
					
				?>
			</table>
		</div>

		<script src="./index_files/bootstrap.min.js"></script>

	</body>
</html>

<?php
	mysqli_close($con);
?>