<?php
	include "conexion.php";
	include "accesaAdmin.php";
	$cve_materia=0;
	$dia=1;
	//$usuario=$_GET['login'];
?>

<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
		<script>
		function submit()
		{
			document.getElementById("forma").submit();
		}
		</script>
	</head>
	<body role="document">
		 <div class="container theme-showcase" role="main">
				<?php 
					$sql="select * from asistencia;";

					$result = mysqli_query($con,$sql);
					$row = mysqli_fetch_array($result); 
					$num_results = mysqli_num_rows($result); 

					if ($num_results==0){
						echo "<h2 style=\"text-align:center;\">No hay asesor&iacute;as registradas.</h2>";
					}
				?>
				<table align="center">
					<tr>
						<td>
							<label>Revisar asesorias de:</label>
						</td>
						<td>&nbsp;</td>
						<td>
							<form action="tablaAsistencia.php" method="post" name="forma">
								<select class="form-control" name="materia" onchange="submit()">
								<?php
									if($cve_materia==0)
										echo "<option selected value='0'>-Nombre-</option>";
									else
										echo "<option value='0'>-Nombre-</option>";
									$sql="select * from materia where status=1 order by nombre;";
									$result = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result))
									{
										if($cve_materia==$row['cve_materia'])
											echo "<option value='".$row['cve_materia']."' selected>".$row['nombre']."</option>";
										else
											echo "<option value='".$row['cve_materia']."'>".$row['nombre']."</option>";
									}
								?>
								</select>
						</td>
						<td>&nbsp;</td>	
						<td>
							<label> D&iacute;a:</label>
						</td>
						<td>&nbsp;</td>	
						<td>
								<select class="form-control" name="dia" onchange="submit()">
								<?php	
									echo "<option value='1'";
									if($dia==1) 
										echo "selected";
									echo">Lunes</option>";
									echo "<option value='2'";
									if($dia==2) 
										echo "selected";
									echo">Martes</option>";
									echo "<option value='3'";
									if($dia==3) 
										echo "selected";
									echo">Miercoles</option>";
									echo "<option value='4'";
									if($dia==4) 
										echo "selected";
									echo">Jueves</option>";
									echo "<option value='5'";
									if($dia==5) 
										echo "selected";
									echo">Viernes</option>";

								?>
								</select>
							</form>
						</td>
					</tr>
				</table>
			</br>
			</br>
			<table class="table table-hover" style="background-color:white;">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>MAE</th>
						<th>Registro</th>
						<th>Tiempo</th>
					</tr>
				</thead>
				<?php
					$sql="SELECT U.nombre, U.aPaterno, U.aMaterno, DATE(A.hora_entrada) AS Fecha, DATE_FORMAT(A.hora_entrada, '%H:%i:%s') AS Hora_Entrada, TIMEDIFF(A.hora_salida, A.hora_entrada) AS TiempoAsesoria
							FROM usuario U, asistencia A
							WHERE U.cve_usuario=A.cve_usuario and A.ensesion = 0
							ORDER BY Fecha DESC, Hora_Entrada";
					$result = mysqli_query($con,$sql);
					$fechaPrevio="";
					while($row = mysqli_fetch_array($result))
					{
						$fechaCompara = $row['Fecha'];

						if($fechaPrevio==$fechaCompara){
								echo "<tr>	
										<td></td>
										<td>".$row['nombre']." ".$row['aPaterno']." ".$row['aMaterno']."</td>
										<td>".$row['Hora_Entrada']."</td>
										<td>".$row['TiempoAsesoria']."</td>
									</tr>";
						}else{
							echo "<tr>	
									<td>".$row['Fecha']."</td>
									<td>".$row['nombre']." ".$row['aPaterno']." ".$row['aMaterno']."</td>
									<td>".$row['Hora_Entrada']."</td>
									<td>".$row['TiempoAsesoria']."</td>
								</tr>";
							$fechaPrevio=$fechaCompara;
						}
					}
				?>
			</table>
		</div>
		<script src="./index_files/bootstrap.min.js"></script>
	</body>
</html>