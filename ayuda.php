<?php
	include "conexion.php";
	include "accesaAdmin.php";
	
?>
<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Diego Delgadillo">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">


		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">
		<link href="css/jumbotton.css" rel="stylesheet">
		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="indexAdmin.php">Bienvenido a MAES</a>
			</div>
			<div>
			  <ul class="nav navbar-nav" style="float:right">
				<li><a href="altaInstructor.php">Instructores</a></li>
				<li><a href="altaMateria.php">Materias</a></li>
				<li><a href="altaCarrera.php">Carreras</a></li>
				<li><a href="borrarBD.php">Borrar base de datos</a></li>
				<li class="active"><a href="ayuda.php">Ayuda</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</div>
		
		<figure style="float:left;margin-top:-20px;position:fixed">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>
		</br></br></br>

		<div class="container">
			<h1 style="text-align:center;min-height:75px;">Ayuda / FAQ</h1>
		</div>

		<div class="container">
			<div class="panel-group" id="accordion">
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
			          <b>Generar un archivo CSV mediante MS Excel.</b>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse">
			      <div class="panel-body">
			      	<ol>
			      		<li>Coloca en pantalla, la hoja de c&aacute;lculo que deseas generar.</li>
			      		<li>Selecciona la opci&oacute;n en la pestaña <b>Archivo > Guardar como</b>.</li>
			      		<li>En la ventana, ubica el folder donde deseas generar el archivo.</li>
			      		<li>Coloca un nombre.</li>
			      		<li>Selecciona la opci&oacute;n de <i>CSV (*.csv)</i> en el apartado Guardar como tipo, ubicado en la parte de abajo.</li>
			      		<li>Haz clic en <b>Guardar</b>.</li>
			      	</ol>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
			          <b>Para dar de alta m&uacute;ltiples MAEs.</b>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse">
			      <div class="panel-body">
			      	<ol>
			      		<li>Coloca los datos de los nuevos MAEs en una hoja de Excel. Asegura que las <i>matriculas</i> aparezcan en la <b>primera columna</b> y su <i>asignaci&oacute;n de ubicaci&oacute;n</i> en la <b>segunda columna</b>. No debe haber t&iacute;tulo de encabezado en cada columna.</li>
			      		<li>Genera y guarda el archivo CSV.</li>
			      		<li>Dentro del portal, accede a la opci&oacute;n <b>Instructores</b>.</li>
			      		<li>Ingresa a la opci&oacute;n <b>A&ntilde;adir m&uacute;ltiples MAEs</b>.</li>
			      		<li>Haz clic en el bot&oacute;n <b>Choose File</b>, ubica el archivo .csv generado anteriormente.</li>
			      		<li>Al terminar, haz clic en <b>Upload</b>, un mensaje de <i>Carga Completa</i> aparecer&aacute; brevemente</b>.</li>
			      	</ol>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
			          <b>Para dar de alta m&uacute;ltiples materias.</b>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse">
			      <div class="panel-body">
			      	<ol>
			      		<li>Coloca las <i>materias</i> en una hoja de Excel. Asegura que aparezcan en la <b>primera columna</b>. No debe haber t&iacute;tulo de encabezado en la columna.</li>
			      		<li>Genera y guarda el archivo CSV.</li>
			      		<li>Dentro del portal, accede a la opci&oacute;n <b>Materias</b>.</li>
			      		<li>Ingresa a la opci&oacute;n <b>A&ntilde;adir m&uacute;ltiples materias</b>.</li>
			      		<li>Haz clic en el bot&oacute;n <b>Choose File</b>, ubica el archivo .csv generado anteriormente.</li>
			      		<li>Al terminar, haz clic en <b>Upload</b>, un mensaje de <i>Carga Completa</i> aparecer&aacute; brevemente</b>.</li>
			      	</ol>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
			          <b>Para dar de alta m&uacute;ltiples carreras.</b>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseFour" class="panel-collapse collapse">
			      <div class="panel-body">
			      	<ol>
			      		<li>Coloca los datos en una hoja de Excel. Asegura que las <i>siglas</i> aparezcan en la <b>primera columna</b> y el <i>nombre</i> en la <b>segunda columna</b>. No debe haber t&iacute;tulo de encabezado en cada columna.</li>
			      		<li>Genera y guarda el archivo CSV.</li>
			      		<li>Dentro del portal, accede a la opci&oacute;n <b>Carreras</b>.</li>
			      		<li>Ingresa a la opci&oacute;n <b>A&ntilde;adir m&uacute;ltiples carreras</b>.</li>
			      		<li>Haz clic en el bot&oacute;n <b>Choose File</b>, ubica el archivo .csv generado anteriormente.</li>
			      		<li>Al terminar, haz clic en <b>Upload</b>, un mensaje de <i>Carga Completa</i> aparecer&aacute; brevemente</b>.</li>
			      	</ol>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
			          <b>Para leer los archivos CSV exportados en MS Excel.</b>
			        </a>
			      </h4>
			    </div>
			    <div id="collapseFive" class="panel-collapse collapse">
			      <div class="panel-body">
			      	<ol>
			      		<li>Abrir MS Excel.</li>
			      		<li>Selecciona la opción en la pestaña <b>Datos > Desde texto</b>.</li>
			      		<li>En la ventana, ubica y selecciona el archivo .csv exportado del sitio.</li>
			      		<li>En la nueva ventana, revisa que la opción <b>Delimitar</b> este marcada; verificar/colocar tambi&eacute;n un <b>1</b> en la casilla, con tal de iniciar el importe de datos desde el primer rengl&oacute;n; en el cuadro <i>Archivo de origen</i>, seleccionar la opci&oacute;n <b>Unicode (UTF-8)</b>. Haga clic en <i>Siguiente</i>.</li>
			      		<li>En la siguiente ventana, verifique que las casillas de <b>Tab</b> y <b>Comma</b> estén seleccionadas. Haga clic en <i>Siguiente</i>.</li>
			      		<li>La última ventana corresponde al formato que se le dar&aacute; a las columnas. Basta con tener la opci&oacute;n <b>General</b> seleccionada. Haga clic en <i>Finalizar</i> para terminar.</li>
			      		<li>Puede hacer el importe en la <i>misma hoja</i> en pantalla o en una <i>nueva</i>.</li>
			      	</ol>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
		
		<script src="./index_files/jquery-1.10.2.min.js"></script>
		<script src="./index_files/bootstrap.min.js"></script>
	</body>
</html>