function checkA1 (obj){
	chk = obj.nextElementSibling.children[0];

	if (!obj.checked){
		chk.checked = false;
		chk.disabled = true;
		obj.parentNode.className = 'none';
	} else {
		chk.disabled = false;
		obj.parentNode.className = 'cetec';
	}

}

function classA1 (obj){
	if (!obj.checked){
		obj.parentNode.parentNode.className = 'cetec';
	} else {
		obj.parentNode.parentNode.className = 'aulas1';
	}
}

(function() {
    var noneItems  = document.querySelectorAll('td.none');

    for (var i = 0; i < noneItems.length; i++){
    	noneItems[i].children[1].children[0].disabled=true
	}

}());