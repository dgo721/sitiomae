<?php
	include "conexion.php";
	include "accesaAdmin.php";
	
?>
<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Diego Delgadillo">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">


		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">
		<link href="css/jumbotton.css" rel="stylesheet">
		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="indexAdmin.php">Bienvenido a MAES</a>
			</div>
			<div>
			  <ul class="nav navbar-nav" style="float:right">
				<li><a href="altaInstructor.php">Instructores</a></li>
				<li><a href="altaMateria.php">Materias</a></li>
				<li class="active"><a href="altaCarrera.php">Carreras</a></li>
				<li><a href="borrarBD.php">Borrar base de datos</a></li>
				<li><a href="ayuda.php">Ayuda</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</div>
		
		<figure style="float:left;margin-top:-20px;position:fixed">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>
		</br></br></br>

		<div class="jumbotron">
			<div class="container" align='center'>
				<h1>Alta m&uacute;ltiple</h1>
				<p class="lead">
					Añadir registro de materias a base de datos mediante archivo CSV.
				</p>
				<?php 
					if (isset($_POST['submit'])) {
						
						mysqli_set_charset($con,"latin1");

						if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
							echo "<h1>Carga completa</h1>";
						}

						$handle = fopen($_FILES['filename']['tmp_name'], "r");

						while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
							
							$sql="INSERT into carrera(siglas,descripcion,status) values('$data[0]','$data[1]','1')";
							mysqli_query($con,$sql) or die(mysql_error());
						}
						fclose($handle);
						mysqli_set_charset($con,"utf8");
						print "Datos almacenados";
					} else {
						print "<form enctype='multipart/form-data' action='carreraMultiple.php' method='post'>";

						print "<h3>Archivo a importar:</h3>\n";

						print "<input type='file' class='btn btn-xs btn-info' name='filename'><br />\n";

						print "<input type='submit' class='btn btn-primary' name='submit' value='Upload'></form>";
						
					}
					

				 ?>
			</div>
		</div>
		
		<script src="./index_files/bootstrap.min.js"></script>
	</body>
</html>
<?php
	mysqli_close($con);
?>