CREATE DATABASE MAES;
USE MAES;
ALTER DATABASE MAES CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE usuario
(
	cve_usuario int NOT NULL AUTO_INCREMENT,
	login varchar(20) NOT NULL UNIQUE,
	password varchar(30) NOT NULL,
	tipo char NOT NULL,
	nombre varchar(60),
	aPaterno varchar(30),
	aMaterno varchar(30),
	ubicacion varchar(20),
	telefono varchar(20),
	correo varchar(50),
	semestre int,
	cve_carrera int,
	PRIMARY KEY(cve_usuario)
);
CREATE TABLE carrera
(
	cve_carrera int NOT NULL AUTO_INCREMENT,
	siglas varchar(10) NOT NULL UNIQUE,
	descripcion varchar(80) NOT NULL UNIQUE,
	status boolean NOT NULL,
	PRIMARY KEY(cve_carrera)
);
CREATE TABLE materia
(
	cve_materia int NOT NULL AUTO_INCREMENT,
	nombre varchar(80) NOT NULL UNIQUE,
	status boolean NOT NULL,
	PRIMARY KEY(cve_materia)
);
CREATE TABLE imparte(
	cve_usuario int NOT NULL,
	cve_materia int NOT NULL,
	promedio int NOT NULL,
	PRIMARY KEY(cve_usuario,cve_materia)
);
CREATE TABLE horario
(
	cve_horario int NOT NULL AUTO_INCREMENT,
	dia int NOT NULL,
	cve_usuario int NOT NULL,
	PRIMARY KEY(cve_horario)
);
CREATE TABLE horariohoras
(
	cve_horario int NOT NULL,
	hora int NOT NULL,
	PRIMARY KEY(cve_horario,hora)
);


USE MAES;
insert into usuario (login,password,tipo,nombre,ubicacion,semestre) values ('admin','amaes456','a','administrador','CETEC 7º piso',0);

insert into Materia (siglas) values ('ITC');
insert into Materia (siglas) values ('IBT');
insert into Materia (siglas) values ('LEC');
insert into Materia (siglas) values ('IMT');
insert into Materia (siglas) values ('LCPF');
insert into Materia (siglas) values ('IQA');
insert into Materia (siglas) values ('IC');
insert into Materia (siglas) values ('LPS');
insert into Materia (siglas) values ('IFI');
insert into Materia (siglas) values ('IIA');
insert into Materia (siglas) values ('LIN');

insert into Usuario (login,password,tipo,nombre,ubicacion,semestre) values ('admin','AMaes456!','a','administrador','CETEC 7º piso',0);

insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01190651','maes651','i','Aldo Stefano','Spatafora','Salazar',8110111211,'A01190651@itesm.mx',6,3);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01150250','maes250','i','Alejandro Gilberto','Hernández','Ramírez',8110111212,'A01150250@itesm.mx',7,3);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00806349','maes349','i','Ana Kelyna','Siliceo','Cuevas',8110111213,'A00806349@itesm.mx',4,4);	
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00809495','maes495','i','Ana Victoria','Balderas','de la Rosa',8110111214,'A00809495@itesm.mx',5,3);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01137878','maes878','i','Ana Victoria','Treviño','Pacheco',8110111215,'A01137878@itesm.mx',3,5);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00811185','maes185','i','Brenda Abigail','Gallegos','Pérez',8110111216,'A00811185@itesm.mx',9,6);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00920015','maes015','i','Celina Andrea','Treviño','Garza',8110111217,'A00920015@itesm.mx',4,4);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00512081','maes081','i','Denise Marión','Seydlitz','Rivera',8110111218,'A00512081@itesm.mx',5,6);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01138036','maes036','i','Diego Armando','de los Reyes','Bocanegra',8110111219,'A01138036@itesm.mx',7,1);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01089909','maes909','i','Diego','Delgadillo','Alvarez',8110111220,'A01089909@itesm.mx',7,1);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01136902','maes902','i','Elsa Cecilia','Chapa','Rodríguez',8110111221,'A01136902@itesm.mx',8,7);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00756599','maes599','i','Francisco','Nava','Morales',8110111222,'A00756599@itesm.mx',8,3);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01150236','maes236','i','Jesús Enrique','Simón','De Anda Cué',8110111223,'A01150236@itesm.mx',9,7);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01190017','maes017','i','Jesús Ramón','Garmendia','Espinoza',8110111224,'A01190017@itesm.mx',4,13);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01111449','maes449','i','Jesús Roberto','Olea','López',8110111225,'A01111449@itesm.mx',5,5);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01195216','maes216','i','Jorge Luis','Hernández','Almaguer',8110111226,'A01195216@itesm.mx',6,8);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00813761','maes761','i','José Luis','Gallegos','Chapa',8110111227,'A00813761@itesm.mx',8,9);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00808854','maes854','i','Juan Sebastián','Lara','García',8110111228,'A00808854@itesm.mx',9,5);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00809460','maes460','i','Lidia Gloria','Solis','Haces',8110111229,'A00809460@itesm.mx',7,4);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00811136','maes136','i','Luis Alfredo','Cantú','Castillo',8110111230,'A00811136@itesm.mx',4,11);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01138259','maes259','i','Mariel Angélica','Reyes','Rendón',8110111231,'A01138259@itesm.mx',5,10);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00809193','maes193','i','Moacyr','Cuervo','Cueva',8110111232,'A00809193@itesm.mx',6,8);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00809380','maes380','i','Ramón','Reyes','Trejo',8110111233,'A00809380@itesm.mx',8,3);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00809037','maes037','i','Raúl Francisco','Treviño','Leal',8110111234,'A00809037@itesm.mx',7,8);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01150878','maes878','i','Ricardo Arturo','López','de la Cruz',8110111235,'A01150878@itesm.mx',7,11);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A01175345','maes345','i','Rodrigo','Martínez','de León',8110111236,'A01175345@itesm.mx',9,1);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00809539','maes539','i','Rómulo Alexander','Sosa','Valdez',8110111237,'A00809539@itesm.mx',4,12);
insert into usuario (login,password,tipo,nombre,aPaterno,aMaterno,telefono,correo,semestre, cve_carrera) values ('A00812190','maes190','a','Silvia Viridiana','Pinto','Huchín',8110111238,'A00812190@itesm.mx',5,3);

USE MAES;
ALTER DATABASE MAES CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE carrera CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
