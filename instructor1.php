<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Andres Cavazos">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">

		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="./">Bienvenido a MAES</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li class="active"><a href="instructor.php">Instructor</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
		
		<figure style="float:left;margin-top:-20px;position:relative">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>
		
		</br></br></br>
		<div class="container">
			<form class="form-horizontal">
				<center><h1>Registro MAE</h1></center><br/>
				<div class="row">
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="inputEmail3">Nombre(s):</label>
						</div>
						<div class="col-xs-9">
							<input name="nombre" type="text" class="form-control" id="inputEmail3" placeholder="Nombre" required>
						</div>
					</div>
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="aPaterno">Paterno:</label>
						</div>
						<div class="col-xs-9">
							<input name="aPaterno" id="aPaterno" type="text" class="form-control" placeholder="Apellido Paterno" required>
						</div>
					</div>
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="aMaterno">Materno:</label>
						</div>
						<div class="col-xs-9">
							<input name="aMaterno" id="aMaterno" type="text" class="form-control" placeholder="Apellido Materno" required>
						</div>
					</div>
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="telefono">Telefono:</label>
						</div>
						<div class="col-xs-9">
							<input name="telefono" id="telefono" type="text" class="form-control" id="inputEmail3" placeholder="Telefono" required>
						</div>
					</div>
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="correo">Correo:</label>
						</div>
						<div class="col-xs-9">
							<input name="correo" id="correo" type="email" class="form-control" placeholder="Correo" required>
						</div>
					</div>
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="carrera">Carrera:</label>
						</div>
						<div class="col-xs-9">
							<select class="form-control" id="carrera" name="carrera">
								<option value=''>-Carrera-</option>
								<option value='1'>ITC</option>
								<option value='2'>ITIC</option>
								<option value='3'>LAD</option>
							</select>
						</div>
					</div>
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="semestre">Semestre:</label>
						</div>
						<div class="col-xs-9">
							<select class="form-control" name="semestre" id="semestre">
								<option value=''>-Semestre-</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</div>
					</div>
				</div>
				<center><h3>Selecciona 3 materias</h3></center>
				<div class="row">
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="mat1">*Materia(1):</label>
						</div>
						<div class="col-xs-9">
							<select class="form-control" name="mat1" id="mat1" onchange="validaMateria()">
								<option value="">-Materia-</option>
								<option value="1">Mate 1</option>
								<option value="2">Mate 2</option>
								<option value="3">Mate 3</option>
							</select>
						</div>
						<div class="col-xs-3">
							<label class="control-label" for="prom1">Promedio:</label>
						</div>
						<div class="col-xs-9">
							<input name="prom1" id="prom1" type="text" class="form-control" placeholder="Promedio" required>
						</div>
					</div>
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="mat2">*Materia(2):</label>
						</div>
						<div class="col-xs-9">
							<select class="form-control" name="mat2" id="mat2" onchange="validaMateria()">
								<option value="">-Materia-</option>
								<option value="1">Mate 1</option>
								<option value="2">Mate 2</option>
								<option value="3">Mate 3</option>
							</select>
						</div>
						<div class="col-xs-3">
							<label class="control-label" for="prom2">Promedio:</label>
						</div>
						<div class="col-xs-9">
							<input name="prom2" id="prom2" type="text" class="form-control" placeholder="Promedio" required>
						</div>
					</div>
					<div class="col-md-4 form-group">
						<div class="col-xs-3">
							<label class="control-label" for="mat3">*Materia(3):</label>
						</div>
						<div class="col-xs-9">
							<select class="form-control" name="mat3" id="mat3" onchange="validaMateria()">
								<option value="">-Materia-</option>
								<option value="1">Mate 1</option>
								<option value="2">Mate 2</option>
								<option value="3">Mate 3</option>
							</select>
						</div>
						<div class="col-xs-3">
							<label class="control-label" for="prom3">Promedio:</label>
						</div>
						<div class="col-xs-9">
							<input name="prom3" id="prom3" type="text" class="form-control" placeholder="Promedio" required>
						</div>
					</div>
				</div>
				<center><h3>Selecciona 5 horas de asesor&iacute;a</h3>
				<div class="row col-md-12 col-md-offset-1">
					<div class="col-md-2">
						<strong>Lunes</strong>
						<div class="row">
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<strong>Martes</strong>
						<div class="row">
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<strong>Miercoles</strong>
						<div class="row">
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<strong>Jueves</strong>
						<div class="row">
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<strong>Viernes</strong>
						<div class="row">
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
							<div class="col-md-12 col-xs-6">
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
								<div class="row"><div class="col-md-8 col-xs-7">8:00-8:30am</div><div class="col-md-4 col-xs-5"><input type="checkbox" value="1"></div></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					</br>
					<button type="submit" class="btn btn-default">Enviar</button>
				</div>
				</center>
			</form>
		</div>
		<script src="./index_files/jquery-1.10.2.min.js"></script>
		<script src="./index_files/bootstrap.min.js"></script>
	</body>
</html>