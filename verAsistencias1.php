<?php
	include "conexion.php";
	include "accesaAdmin.php";
	
	//if(!isset($_GET['exportar'])){
?>


<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Andres Cavazos">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">


		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">

		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="indexAdmin.php">Bienvenido a MAES</a>
			</div>
			<div>
			  <ul class="nav navbar-nav" style="float:right">
				<li class="active"><a href="verAsistencias.php">Asistencias</a></li>
				<li><a href="altaInstructor.php">Instructores</a></li>
				<li><a href="altaMateria.php">Materias</a></li>
				<li><a href="altaCarrera.php">Carreras</a></li>
				<li><a href="borrarBD.php">Borrar base de datos</a></li>
				<li><a href="ayuda.php">Ayuda</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
			  </ul>
			</div>
		  </div>
		</div>
		
		<figure style="float:left;margin-top:-20px;position:static">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>
		
		<center>
			<?php 
					$sql="select * from asistencia;";

					$result = mysqli_query($con,$sql);
					$row = mysqli_fetch_array($result); 
					$num_results = mysqli_num_rows($result); 

					if ($num_results==0){
						echo "<h2 style=\"text-align:center;\">No hay asistencias registradas.</h2>";
					} else {
						echo "<h2 style=\"text-align:center;\">Registros de Asistencia</h2>";
					}
				?>
				<table align="center">
				
				</table>
		</center>
		</br>
		<div class="container">
			<table class="table table-hover" style="background-color:white;">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>MAE</th>
						<th>Entrada</th>
						<th>Tiempo (h/m/s)</th>
					</tr>
				</thead>
				<?php
					$sql="SELECT U.nombre, U.aPaterno, U.aMaterno, DATE_FORMAT( A.hora_entrada,  '%d-%m-%Y' ) AS Fecha, DATE_FORMAT( A.hora_entrada,  '%H:%i:%s %p' ) AS Hora_Entrada, TIMEDIFF( A.hora_salida, A.hora_entrada ) AS TiempoAsesoria
							FROM usuario U, asistencia A
							WHERE U.cve_usuario = A.cve_usuario
							AND A.ensesion =0
							ORDER BY YEAR( A.hora_entrada ) DESC , MONTH( A.hora_entrada ) DESC , DAY( A.hora_entrada ) DESC , Hora_Entrada";
					$result = mysqli_query($con,$sql);
					$fechaPrevio="";
					while($row = mysqli_fetch_array($result))
					{
						$fechaCompara = $row['Fecha'];

						if($fechaPrevio==$fechaCompara){
								echo "<tr>	
										<td></td>
										<td>".$row['nombre']." ".$row['aPaterno']." ".$row['aMaterno']."</td>
										<td>".$row['Hora_Entrada']."</td>
										<td>".$row['TiempoAsesoria']."</td>
									</tr>";
						}else{
							echo "<tr>	
									<td>".$row['Fecha']."</td>
									<td>".$row['nombre']." ".$row['aPaterno']." ".$row['aMaterno']."</td>
									<td>".$row['Hora_Entrada']."</td>
									<td>".$row['TiempoAsesoria']."</td>
								</tr>";
							$fechaPrevio=$fechaCompara;
						}
					}
				?>
			</table>
		</div>
		<script src="./index_files/bootstrap.min.js"></script>

	</body>
</html>

<?php
	mysqli_close($con);
?>