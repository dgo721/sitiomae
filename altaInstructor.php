<?php
	include "conexion.php";
	include "accesaAdmin.php";
	
	
	$matricula="";
	if(isset($_POST["matricula"])){
		$matricula=$_POST["matricula"];
		$cve=$_POST["clave"];
		$lugar=1;
		//$lugar=$_POST["lugar"];
		if ($lugar==1):
			$ubica="CETEC 7-piso";
		elseif ($lugar==2):
			$ubica="Aulas 1";
		endif;
		$sql="INSERT INTO usuario (login, password, tipo, ubicacion) VALUES ('".$matricula."','".$cve."', 'i','".$ubica."')";
		if (!mysqli_query($con,$sql)){
			echo "<script language=\"javascript\">
					alert(\"Instructor no pudo ser agregado: Matricula duplicada\");
				</script>";
		}else{
			echo "<script language=\"javascript\">
						alert(\"Instructor Agregado con exito!\");
					</script>";
		}
	}
	
	$sql="SELECT 
					*
				FROM
					usuario u left join carrera c on  u.cve_carrera = c.cve_carrera
				where
					 u.tipo = 'i' ";
	$buscarPor="";
	if(isset( $_GET['buscarPor'])){
		$buscarPor=$_GET['buscarPor'];
		$buscarContenga=$_GET['buscarContenga'];
		switch($buscarPor){
			case "matricula":
				$sql.="and login like '%$buscarContenga%'";
				break;
			case "carrera":
				$sql.="and siglas like '%$buscarContenga%' or descripcion like '%$buscarContenga%'";
				break;
			case "nombre":
				$sql.="and nombre like '%$buscarContenga%'";
				break;
			case "aPaterno":
				$sql.="and aPaterno like '%$buscarContenga%'";
				break;
			case "aMaterno":
				$sql.="and aMaterno like '%$buscarContenga%' ";
				break;
		}
	}
	$sql.="order by ISNULL(nombre), nombre ASC";
	
	if(!isset($_GET['exportar'])){
?>


<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Andres Cavazos">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">


		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">

		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="indexAdmin.php">Bienvenido a MAES</a>
			</div>
			<div>
			  <ul class="nav navbar-nav" style="float:right">
				<li class="active"><a href="altaInstructor.php">Instructores</a></li>
				<li><a href="altaMateria.php">Materias</a></li>
				<li><a href="altaCarrera.php">Carreras</a></li>
				<li><a href="borrarBD.php">Borrar base de datos</a></li>
				<li><a href="ayuda.php">Ayuda</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
			  </ul>
			</div>
		  </div>
		</div>
		
		<figure style="float:left;margin-top:-20px;position:static">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>
		
		<center>
			<form class="form-inline" role="form" method="get" action='altaInstructor.php'>
				<table>
					<tr>
						<td>
							<h3>Buscar por:&nbsp;</h3>
						</td>
						<td>	
							<select class="form-control" name="buscarPor">
								<option value="nombre" <?php if($buscarPor=="nombre")echo "selected";?>>Nombre</option>
								<option value="aPaterno" <?php if($buscarPor=="aPaterno")echo "selected";?>>Apellido Paterno</option>
								<option value="aMaterno" <?php if($buscarPor=="aMaterno")echo "selected";?>>Apellido Materno</option>
								<option value="carrera" <?php if($buscarPor=="carrera")echo "selected";?>>Carrera</option>
								<option value="matricula" <?php if($buscarPor=="matricula")echo "selected";?>>Matricula</option>
							</select>						
						</td>
						<td>
							&nbsp;&nbsp;
						</td>
						<td>
							<h3>Que contenga:&nbsp;</h3>
						</td>
						<td>
							<input type="text" class="form-control" name="buscarContenga" <?php if(isset($buscarContenga)) echo "value=\"$buscarContenga\""?>>
						</td>
						<td>
							&nbsp;<button type="submit" class="btn btn-default">Buscar</button>
						</td>
						<td>
							&nbsp;<button type="submit" class="btn btn-default" name="exportar" value="1">Exportar</button>
						</td>
					</tr>
				</table>
			</form>
		</center>
		</br>
		<div class="container">
			<table class="table table-hover" style="background-color:white;">
				<thead>
					<tr>
						<th>Matricula</th>
						<th>Nombre</th>
						<th>Apellido Paterno</th>
						<th>Apellido Materno</th>
						<th>Telefono</th>
						<th>Correo</th>
						<th>Carrera</th>
						<th>Semestre</th>
					</tr>
				</thead>
				<?php
					$result = mysqli_query($con,$sql);

					if ($result!=""){
						while($row = mysqli_fetch_array($result)){
	
							echo "<tr>
										<td><a href=\"instructorAdmin.php?login=".$row['login']."\">".$row['login']."</a></td>
										<td>".$row['nombre']."</td>
										<td>".$row['aPaterno']."</td>
										<td>".$row['aMaterno']."</td>
										<td>".$row['telefono']."</td>
										<td>".$row['correo']."</td>
										<td>".$row['siglas']."</td>
										<td>".$row['semestre']."</td>
									</tr>";
						}
					} else {
						echo "<tr>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
										<td>---</td>
									</tr>";
					}
					
				?>
			</table>
		</div>
		<center>

			<?php
			$sql="SELECT COUNT(*) as 'todos' FROM usuario";
			$result = mysqli_query($con,$sql);
			$totales = mysqli_fetch_array($result);
			$numtotal = $totales['todos'];

			$sql="SELECT COUNT(*) as 'enregistro' FROM usuario WHERE nombre is not NULL";
			$result = mysqli_query($con,$sql);
			$registrados = mysqli_fetch_array($result);
			$numregistra = $registrados['enregistro'];

			echo "<h4>MAES registrados: <i>".$numregistra."</i> / <b>".$numtotal."</b></h4>"
			?>

			<form class="form-inline" role="form" action="altaInstructor.php" method="post">
				<table>
					<tr>
						<td>
							<h3>Agregar MAE: </h3>
						</td>
						<td>
							<label class="sr-only" for="exampleInputEmail2">Matricula</label>
							<input name="matricula" type="text" class="form-control" id="exampleInputEmail2" placeholder="Matricula" required>
							<label class="sr-only" for="exampleInputPassword2">Clave</label>
							<input name="clave" type="text" class="form-control" id="exampleInputPassword2" placeholder="Clave" required>
							<!--
							<select class="form-control" name="lugar">
								<option selected value='0'>-Lugar de Asesor&iacute;a-</option>
								<option value='1'>CETEC 7-piso</option>
								<option value='2'>Aulas 1</option>
							</select>
							-->
							<button type="submit" class="btn btn-default">Guardar</button>
						</td>
					</tr>
				</table>
			</form>
			<h4 ><a class="btn btn-lg btn-primary" href="altaMultiple.php">Añadir m&uacute;ltiples MAEs</a></h4>
		</center>
		<script src="./index_files/bootstrap.min.js"></script>

	</body>
</html>
<?php
}else{
	if($_GET['exportar']==1){

		// filename for download
		$filename = "instructor" . date('Ymd') . ".csv";

		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: text/csv; charset=UTF-16LE");

		$out = fopen("php://output", 'w');

		$flag = false;
		$result = mysqli_query($con,$sql);
		while($row = mysqli_fetch_array($result)) {
			if(!$flag) {
				// display field/column names as first row
				$titulos=array("cve_usuario","Matricula","Clave","Tipo","Nombre",
								"aPaterno","aMaterno","Telefono","Correo","Semestre",
								"cve_carrera","Siglas","Descripcion","Estatus");
				fputcsv($out,$titulos);
				$flag = true;
			}
			//array_walk($row, 'cleanData');
			$contenido=array($row['cve_usuario'],$row['login'],$row['password'],$row['tipo'],$row['nombre'],
								$row['aPaterno'],$row['aMaterno'],$row['telefono'],$row['correo'],$row['semestre'],
								$row['cve_carrera'],$row['siglas'],$row['descripcion'],$row['status']);
			fputcsv($out, $contenido);
		}
		fclose($out);
	}
}	
?>
<?php
	mysqli_close($con);
?>