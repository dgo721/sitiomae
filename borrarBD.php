<?php
	include "conexion.php";
	include "accesaAdmin.php";
	
	if(isset($_GET["accion"])){
		if($_GET["accion"]=="borrar"){
			echo "<script language=\"javascript\">
					var r=confirm(\"¿Esta seguro de esto?\");
					if (r==true){
						window.location.href = \"borrarBD.php?accion=si\"
					}else{
						window.location.href = \"borrarBD.php\"
					 }
					</script>";
		}else if($_GET["accion"]=="si"){
			$sql="DELETE FROM usuario where tipo ='i'";
			mysqli_query($con,$sql);
			$sql="DELETE FROM horariohoras";
			mysqli_query($con,$sql);
			$sql="DELETE FROM horario";
			mysqli_query($con,$sql);
			$sql="DELETE FROM imparte";
			mysqli_query($con,$sql);
			echo "<script language=\"javascript\">
						alert(\"Base de datos borrada con exito\")
					</script>";
		}
	}
	
?>
<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Andres Cavazos">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">


		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">
		<link href="css/jumbotton.css" rel="stylesheet">
		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="indexAdmin.php">Bienvenido a MAES</a>
			</div>
			<div>
			  <ul class="nav navbar-nav" style="float:right">
				<li><a href="altaInstructor.php">Instructores</a></li>
				<li><a href="altaMateria.php">Materias</a></li>
				<li><a href="altaCarrera.php">Carreras</a></li>
				<li class="active"><a href="borrarBD.php">Borrar base de datos</a></li>
				<li><a href="ayuda.php">Ayuda</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</div>
		
		<figure style="float:left;margin-top:-20px;position:fixed">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>
		</br></br></br>

		<div class="jumbotron">
			<h1>Borrar Base de Datos</h1>
			<p class="lead">
				Eliminaci&oacute;n de base de datos de instructores (Materias y Carreras no son afectadas).
			</p>
			<p>
				<a class="btn btn-lg btn-success" href="borrarBD.php?accion=borrar" role="button">Borrar</a>
			</p>
		</div>
		
		<script src="./index_files/bootstrap.min.js"></script>
	</body>
</html>
<?php
	mysqli_close($con);
?>