<?php
	include "conexion.php";
	include "accesaInstructor.php";

	function debug_to_console( $data ) {

	  if ( is_array( $data ) )
	    $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
	  else
	    $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

	  echo $output;
	}

	function debug_to_alert( $data ) {

	  if ( is_array( $data ) )
	    $output = "<script>alert( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
	  else
	    $output = "<script>alert( 'Debug Objects: " . $data . "' );</script>";

	  echo $output;
	}

	$usuario=$_GET['login'];
	$sql="SELECT 
				* 
			FROM 
				usuario 
			where 
				login='$usuario'";
	$result = mysqli_query($con,$sql);
	while($row = mysqli_fetch_array($result)){
		$nombreInst=$row['nombre'];
		$aPaternoInst=$row['aPaterno'];
		$aMaternoInst=$row['aMaterno'];
		$telefonoInst=$row['telefono'];
		$correoInst=$row['correo'];
		$semestreInst=$row['semestre'];
		$carreraInst=$row['cve_carrera'];
		$cve_usuario=$row['cve_usuario'];
	}
	
	$sql="SELECT 
			i.cve_materia,
			promedio,
			nombre
		FROM
			imparte i,
			materia m
		where
			cve_usuario = $cve_usuario
			and m.cve_materia=i.cve_materia
			and m.status=1
		order by nombre;";
	$result = mysqli_query($con,$sql);
	$cont=0;
	while($row = mysqli_fetch_array($result)){
		$cveMateriaInst[$cont]=$row['cve_materia'];
		$promedioInst[$cont]=$row['promedio'];
		$nomMatInst[$cont]=$row['nombre'];
		$cont++;
	}	

	if(isset($_POST["nombre"])){
				
		$N1 = 0;
		$N2 = 0;
		$N3 = 0;
		$N4 = 0;
		$N5 = 0;

		$M1 = 0;
		$M2 = 0;
		$M3 = 0;
		$M4 = 0;
		$M5 = 0;
		
		//Revisar si se selecciono algun dia para que no regrese error.
		if(isset ($_POST["hora0"])){
			$dia1 = $_POST["hora0"];
			$N1 = count($dia1);
		}
		if(isset ($_POST["hora1"])){
			$dia2 = $_POST["hora1"];
			$N2 = count($dia2);
		}
		if(isset ($_POST["hora2"])){
			$dia3 = $_POST["hora2"];
			$N3 = count($dia3);
		}
		if(isset ($_POST["hora3"])){
			$dia4 = $_POST["hora3"];
			$N4 = count($dia4);
		}
		if(isset ($_POST["hora4"])){
			$dia5 = $_POST["hora4"];
			$N5 = count($dia5);
		}

		//Selecciones de horario en Aulas 1
		if(isset ($_POST["sede0"])){
			$a1_1 = $_POST["sede0"];
			$M1 = count($a1_1);
		}
		if(isset ($_POST["sede1"])){
			$a1_2 = $_POST["sede1"];
			$M2 = count($a1_2);
			debug_to_console($a1_2);
			debug_to_console($M2);
		}
		if(isset ($_POST["sede2"])){
			$a1_3 = $_POST["sede2"];
			$M3 = count($a1_3);
		}
		if(isset ($_POST["sede3"])){
			$a1_4 = $_POST["sede3"];
			$M4 = count($a1_4);
		}
		if(isset ($_POST["sede4"])){
			$a1_5 = $_POST["sede4"];
			$M5 = count($a1_5);
		}
		
		if($N1+$N2+$N3+$N4+$N5<2){
			echo "<script language=\"javascript\">
					alert(\"Se debe registrar por lo menos 1 hora!\");
				</script>";
		}else{			
			//Recibo los datos del Instructor
			$nombre=$_POST["nombre"];
			$aPaterno=$_POST["aPaterno"];
			$aMaterno=$_POST["aMaterno"];
			$telefono=$_POST["telefono"];
			$correo=$_POST["correo"];
			$carrera=$_POST["carrera"];
			$semestre=$_POST["semestre"];
			
			//Recibo todas las materias con sus promedios
			$mat1=$_POST["mat1"];
			$prom1=$_POST["prom1"];
			
			if($mat1!=0){

				$mat2=$_POST["mat2"];
				$prom2=$_POST["prom2"];

				$mat3=$_POST["mat3"];
				$prom3=$_POST["prom3"];

				$mat4=$_POST["mat4"];
				$prom4=$_POST["prom4"];

				$mat5=$_POST["mat5"];
				$prom5=$_POST["prom5"];
				
				$mat6=$_POST["mat6"];
				$prom6=$_POST["prom6"];
				
				$mat7=$_POST["mat7"];
				$prom7=$_POST["prom7"];
				
				$mat8=$_POST["mat8"];
				$prom8=$_POST["prom8"];
				
				$mat9=$_POST["mat9"];
				$prom9=$_POST["prom9"];
				
				$mat10=$_POST["mat10"];
				$prom10=$_POST["prom10"];
				
				
				// Se registran datos de usuario	
				$sqlUsuario="UPDATE usuario set 
								nombre='$nombre', 
								aPaterno='$aPaterno', 
								aMaterno='$aMaterno',
								telefono='$telefono',
								correo='$correo',
								semestre='$semestre',
								cve_carrera='$carrera',
								password='INSMaes123!'
							where 
								login='$usuario'";
				
				$sqlMat1="INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat1','$prom1')";
				
				mysqli_query($con,"DELETE FROM imparte where cve_usuario='$cve_usuario'") or die('Error:'. mysqli_error($con));
				
				$result = mysqli_query($con,"Select cve_horario FROM horario where cve_usuario='$cve_usuario'") or die('Error:'. mysqli_error($con));
				while($row = mysqli_fetch_array($result)){
					mysqli_query($con,"DELETE FROM horariohoras where cve_horario='".$row['cve_horario']."'") or die('Error:'. mysqli_error($con));
				}
				mysqli_query($con,"DELETE FROM horario where cve_usuario='$cve_usuario'") or die('Error:'. mysqli_error($con));
				
				if($mat2!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat2','$prom2')") or die('Error:'. mysqli_error($con));
				if($mat3!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat3','$prom3')") or die('Error:'. mysqli_error($con));
				if($mat4!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat4','$prom4')") or die('Error:'. mysqli_error($con));
				if($mat5!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat5','$prom5')") or die('Error:'. mysqli_error($con));
				if($mat6!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat6','$prom6')") or die('Error:'. mysqli_error($con));
				if($mat7!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat7','$prom7')") or die('Error:'. mysqli_error($con));
				if($mat8!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat8','$prom8')") or die('Error:'. mysqli_error($con));
				if($mat9!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat9','$prom9')") or die('Error:'. mysqli_error($con));
				if($mat10!=0)
					mysqli_query($con,"INSERT INTO imparte (cve_usuario,cve_materia,promedio) VALUES ('$cve_usuario','$mat10','$prom10')") or die('Error:'. mysqli_error($con));
				
				mysqli_query($con,$sqlUsuario) or die('Error:'. mysqli_error($con));
				mysqli_query($con,$sqlMat1) or die('Error:'. mysqli_error($con));
				
				if($N1!=0){
					mysqli_query($con,"INSERT INTO horario (dia,cve_usuario) VALUES ('1','$cve_usuario')") or die('Error:'. mysqli_error($con));
					$sql="select cve_horario from horario where dia=1 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $N1; $i++){
							mysqli_query($con,"INSERT INTO horariohoras (cve_horario,hora) VALUES ('".$row['cve_horario']."','".$dia1[$i]."')") or die('Error:'. mysqli_error($con));
						}
					}
				}
				if($N2!=0){
					mysqli_query($con,"INSERT INTO horario (dia,cve_usuario) VALUES ('2','$cve_usuario')") or die('Error:'. mysqli_error($con));
					$sql="select cve_horario from horario where dia=2 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $N2; $i++){
							mysqli_query($con,"INSERT INTO horariohoras (cve_horario,hora) VALUES ('".$row['cve_horario']."','".$dia2[$i]."')") or die('Error:'. mysqli_error($con));
						}
					}
				}
				if($N3!=0){
					mysqli_query($con,"INSERT INTO horario (dia,cve_usuario) VALUES ('3','$cve_usuario')") or die('Error:'. mysqli_error($con));
					$sql="select cve_horario from horario where dia=3 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $N3; $i++){
							mysqli_query($con,"INSERT INTO horariohoras (cve_horario,hora) VALUES ('".$row['cve_horario']."','".$dia3[$i]."')") or die('Error:'. mysqli_error($con));
						}
					}
				}
				if($N4!=0){
					mysqli_query($con,"INSERT INTO horario (dia,cve_usuario) VALUES ('4','$cve_usuario')") or die('Error:'. mysqli_error($con));
					$sql="select cve_horario from horario where dia=4 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $N4; $i++){
							mysqli_query($con,"INSERT INTO horariohoras (cve_horario,hora) VALUES ('".$row['cve_horario']."','".$dia4[$i]."')") or die('Error:'. mysqli_error($con));
						}
					}
				}
				if($N5!=0){
					mysqli_query($con,"INSERT INTO horario (dia,cve_usuario) VALUES ('5','$cve_usuario')") or die('Error:'. mysqli_error($con));
					$sql="select cve_horario from horario where dia=5 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $N5; $i++){
							mysqli_query($con,"INSERT INTO horariohoras (cve_horario,hora) VALUES ('".$row['cve_horario']."','".$dia5[$i]."')") or die('Error:'. mysqli_error($con));
						}
					}
				}


				if($M1!=0){
					$sql="select cve_horario from horario where dia=1 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $M1; $i++){
							debug_to_console($a1_1[$i]);
							mysqli_query($con,"UPDATE horariohoras set ubica=1 where cve_horario='".$row['cve_horario']."' and hora='".$a1_1[$i]."'") or die('Error:'. mysqli_error($con));
						}
					}
				}
				
				if($M2!=0){
					$sql="select cve_horario from horario where dia=2 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $M2; $i++){
							debug_to_console($a1_2[$i]);
							mysqli_query($con,"UPDATE horariohoras set ubica=1 where cve_horario='".$row['cve_horario']."' and hora='".$a1_2[$i]."'") or die('Error:'. mysqli_error($con));
						}
					}
				}

				if($M3!=0){
					$sql="select cve_horario from horario where dia=3 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $M3; $i++){
							debug_to_console($a1_3[$i]);
							mysqli_query($con,"UPDATE horariohoras set ubica=1 where cve_horario='".$row['cve_horario']."' and hora='".$a1_3[$i]."'") or die('Error:'. mysqli_error($con));
						}
					}
				}

				if($M4!=0){
					$sql="select cve_horario from horario where dia=3 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $M4; $i++){
							debug_to_console($a1_4[$i]);
							mysqli_query($con,"UPDATE horariohoras set ubica=1 where cve_horario='".$row['cve_horario']."' and hora='".$a1_4[$i]."'") or die('Error:'. mysqli_error($con));
						}
					}
				}

				if($M5!=0){
					$sql="select cve_horario from horario where dia=3 and cve_usuario=$cve_usuario;";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result)){
						for($i=0; $i < $M5; $i++){
							debug_to_console($a1_5[$i]);
							mysqli_query($con,"UPDATE horariohoras set ubica=1 where cve_horario='".$row['cve_horario']."' and hora='".$a1_5[$i]."'") or die('Error:'. mysqli_error($con));
						}
					}
				}
				
				echo "<script language=\"javascript\">
							alert(\"Datos registrados con exito!\");
							window.location.href = \"altaInstructor.php\"
						</script>";
			}else{
				echo "<script language=\"javascript\">
						alert(\"Se debe registrar por lo menos 1 materia!\");
					</script>";
			}
		}
	}
?>
<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Andres Cavazos">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">


		<style type="text/css">
			
			#radioselect {
				float: right;
				display: inline;
			}

			span.cetec {
				background-color: #c9caf2;}

			span.aulas1 {
				background-color: #bbe3aa;}

			td.cetec {
				background-color: #c9caf2;}

			td.aulas1 {
				background-color: #bbe3aa;}

			td:hover {
				background-color: #c3e6e5;}
		</style>
		<style id="holderjs-style" type="text/css"></style>
		<script>
		function checaPromedio(){
			var mat1=document.getElementById("mat1").value;
			var mat2=document.getElementById("mat2").value;
			var mat3=document.getElementById("mat3").value;
			var mat4=document.getElementById("mat4").value;
			var mat5=document.getElementById("mat5").value;
			var mat6=document.getElementById("mat6").value;
			var mat7=document.getElementById("mat7").value;
			var mat8=document.getElementById("mat8").value;
			var mat9=document.getElementById("mat9").value;
			var mat10=document.getElementById("mat10").value;
			var x=0;
			if(mat1!=0){
				if(document.getElementById("prom1").value==0 || document.getElementById("prom1").value==null){
					alert("Promedio de materia 1 no ingresado");
					x=1;
				}
			}
			if(mat2!=0){
				if(document.getElementById("prom2").value==0 || document.getElementById("prom2").value==""){
					alert("Promedio de materia 2 no ingresado");
					x=1;
				}
			}
			if(mat3!=0){
				if(document.getElementById("prom3").value==0 || document.getElementById("prom3").value==""){
					alert("Promedio de materia 3 no ingresado");
					x=1;
				}
			}
			if(mat4!=0){
				if(document.getElementById("prom4").value==0 || document.getElementById("prom4").value==""){
					alert("Promedio de materia 4 no ingresado");
					x=1;
				}
			}
			if(mat5!=0){
				if(document.getElementById("prom5").value==0 || document.getElementById("prom5").value==""){
					alert("Promedio de materia 5 no ingresado");
					x=1;
				}
			}
			if(mat6!=0){
				if(document.getElementById("prom6").value==0 || document.getElementById("prom6").value==""){
					alert("Promedio de materia 6 no ingresado");
					x=1;
				}
			}
			if(mat7!=0){
				if(document.getElementById("prom7").value==0 || document.getElementById("prom7").value==""){
					alert("Promedio de materia 7 no ingresado");
					x=1;
				}
			}
			if(mat8!=0){
				if(document.getElementById("prom8").value==0 || document.getElementById("prom8").value==""){
					alert("Promedio de materia 8 no ingresado");
					x=1;
				}
			}
			if(mat9!=0){
				if(document.getElementById("prom9").value==0 || document.getElementById("prom9").value==""){
					alert("Promedio de materia 9 no ingresado");
					x=1;
				}
			}
			if(mat10!=0){
				if(document.getElementById("prom10").value==0 || document.getElementById("prom10").value==""){
					alert("Promedio de materia 10 no ingresado");
					x=1;
				}
			}
			if(x)
				return false;
			else
				return true;
		}
		function validaMateria(){
			var mat1=document.getElementById("mat1").value;
			var mat2=document.getElementById("mat2").value;
			var mat3=document.getElementById("mat3").value;
			var mat4=document.getElementById("mat4").value;
			var mat5=document.getElementById("mat5").value;
			var mat6=document.getElementById("mat6").value;
			var mat7=document.getElementById("mat7").value;
			var mat8=document.getElementById("mat8").value;
			var mat9=document.getElementById("mat9").value;
			var mat10=document.getElementById("mat10").value;
			var tof=1;
			var cambio=0;
			if(mat1!=0){
				if( mat1!=mat2 && mat1!=mat3 && mat1!=mat4 && mat1!=mat5 && mat1!=mat6 && mat1!=mat7 && mat1!=mat8 && mat1!=mat9 && mat1!=mat10){
				}else{
					tof=0;
					cambio=1;
				}
			}
			if(mat2!=0){
				if(mat1!=mat2 && mat2!=mat3 && mat2!=mat4 && mat2!=mat5 && mat2!=mat6 && mat2!=mat7 && mat2!=mat8 && mat2!=mat9 && mat2!=mat10){
				}else{
					tof=0;
					cambio=2;
				}
			}
			if(mat3!=0){ 
				if(mat3!=mat1 && mat3!=mat2 && mat3!=mat4 && mat3!=mat5 && mat3!=mat6 && mat3!=mat7 && mat3!=mat8 && mat3!=mat9 && mat3!=mat10){
				}else{
					tof=0;
					cambio=3;
				}
			}
			if(mat4!=0){
				if(mat4!=mat1 && mat4!=mat2 && mat4!=mat3 && mat4!=mat5 && mat4!=mat6 && mat4!=mat7 && mat4!=mat8 && mat4!=mat9 && mat4!=mat10){
				}else{
					tof=0;
					cambio=4;
				}
			}
			if(mat5!=0){
				if(mat1!=mat5 && mat5!=mat2 && mat5!=mat3 && mat5!=mat4 && mat5!=mat6 && mat5!=mat7 && mat5!=mat8 && mat5!=mat9 && mat5!=mat10){
				}else{
					tof=0;
					cambio=5;
				}
			}
			if(mat6!=0){
				if(mat1!=mat6 && mat6!=mat2 && mat6!=mat3 && mat6!=mat4 && mat6!=mat5 && mat6!=mat7 && mat6!=mat8 && mat6!=mat9 && mat6!=mat10){
				}else{
					tof=0;
					cambio=6;
				}
			}
			if(mat7!=0){
				if(mat1!=mat7 && mat7!=mat2 && mat7!=mat3 && mat7!=mat4 && mat7!=mat5 && mat7!=mat6 && mat7!=mat8 && mat7!=mat9 && mat7!=mat10){
				}else{
					tof=0;
					cambio=7;
				}
			}
			if(mat8!=0){
				if(mat1!=mat8 && mat8!=mat2 && mat8!=mat3 && mat8!=mat4 && mat8!=mat5 && mat8!=mat6 && mat8!=mat7 && mat8!=mat9 && mat8!=mat10){
				}else{
					tof=0;
					cambio=8;
				}
			}
			if(mat9!=0){
				if(mat1!=mat9 && mat9!=mat2 && mat9!=mat3 && mat9!=mat4 && mat9!=mat5 && mat9!=mat6 && mat9!=mat7 && mat9!=mat8 && mat9!=mat10){
				}else{
					tof=0;
					cambio=9;
				}
			}
			if(mat10!=0){
				if(mat1!=mat10 && mat10!=mat2 && mat10!=mat3 && mat10!=mat4 && mat10!=mat5 && mat10!=mat6 && mat10!=mat7 && mat10!=mat8 && mat10!=mat9){
				}else{
					tof=0;
					cambio=10;
				}
			}
			
			if(tof==0)
				alert("Seleccione otra materia por favor");
				
			switch(cambio){
				case 1:
					document.getElementById("mat1").value=0;
					document.getElementById("prom1").value=0;
					break;
				case 2:
					document.getElementById("mat2").value=0;
					document.getElementById("prom2").value=0;
					break;
				case 3:
					document.getElementById("mat3").value=0;
					document.getElementById("prom3").value=0;
					break;
				case 4:
					document.getElementById("mat4").value=0;
					document.getElementById("prom4").value=0;
					break;
				case 5:
					document.getElementById("mat5").value=0;
					document.getElementById("prom5").value=0;
					break;
				case 6:
					document.getElementById("mat6").value=0;
					document.getElementById("prom6").value=0;
					break;
				case 7:
					document.getElementById("mat7").value=0;
					document.getElementById("prom7").value=0;
					break;
				case 8:
					document.getElementById("mat8").value=0;
					document.getElementById("prom8").value=0;
					break;
				case 9:
					document.getElementById("mat9").value=0;
					document.getElementById("prom9").value=0;
					break;
				case 10:
					document.getElementById("mat10").value=0;
					document.getElementById("prom10").value=0;
					break;
			}
		}
		</script>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="indexAdmin.php">Bienvenido a MAES</a>
			</div>
			<div>
			  <ul class="nav navbar-nav" style="float:right">
				<li class="active"><a href="altaInstructor.php">Instructores</a></li>
				<li><a href="altaMateria.php">Materias</a></li>
				<li><a href="altaCarrera.php">Carreras</a></li>
				<li><a href="borrarBD.php">Borrar base de datos</a></li>
				<li><a href="ayuda.php">Ayuda</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
			  </ul>
			</div>
		  </div>
		</div>
		
		<figure style="float:left;margin-top:-20px;position:relative">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>

		<form class="form-horizontal" role="form" method="post" onsubmit="checaPromedio();">
		<center>
			<h1>Registrar Instructor</h1>
			</br>
			<table border="0">
				<tr>
					<td>
						<label class="col-sm-2 control-label">Nombre(s):</label>
					</td>
					<td>
						<input name="nombre" type="text" class="form-control" id="inputEmail3" value="<?php echo $nombreInst;?>" required>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Paterno:</label>
					</td>
					<td>
						<input name="aPaterno" type="text" class="form-control" value="<?php echo $aPaternoInst;?>" required>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Materno:</label>
					</td>
					<td>
						<input name="aMaterno" type="text" class="form-control" value="<?php echo $aMaternoInst;?>" required>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<label class="col-sm-2 control-label">Telefono:</label>
					</td>
					<td>
						<input name="telefono" type="text" class="form-control" id="inputEmail3" value="<?php echo $telefonoInst;?>" required>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Correo:</label>
					</td>
					<td>
						<input name="correo" type="email" class="form-control" value="<?php echo $correoInst;?>" required>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Carrera:</label>
					</td>
					<td>
						<select class="form-control" name="carrera">
								<option value='0'>-Carrera-</option>
							<?php

								$sql="select * from carrera where status=1 order by siglas;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result))
								{
									if($carreraInst==$row['cve_carrera']){
										echo "<option value='".$row['cve_carrera']."' selected>".$row['siglas']."</option>";
									}else{
										echo "<option value='".$row['cve_carrera']."'>".$row['siglas']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Semestre:</label>
					</td>
					<td>
						<select class="form-control" name="semestre">
							<?php 
								for($i=1;$i<=10;$i++){
									if($i==$semestreInst){
										echo "<option value=\"$i\" selected>$i</option>";
									}else{
										echo "<option value=\"$i\">$i</option>";
									}
								}
							?>"
						</select>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<h3>Se debe seleccionar 1 materia m&iacute;nimo</h3>
			<table>
				<tr>
					<td>
						<label class="col-sm-2 control-label">*Materia(1):</label>
					</td>
					<td>
						<select class="form-control" name="mat1" id="mat1" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[0])){
										if($cveMateriaInst[0]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom1" id="prom1" type="text" class="form-control" value="<?php if(isset($promedioInst[0])) echo $promedioInst[0]?>" required>
					</td>
					<td>
						<label class="col-sm-2 control-label">*Materia(2):</label>
					</td>
					<td>
						<select class="form-control" name="mat2"  id="mat2" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[1])){
										if($cveMateriaInst[1]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom2" id="prom2" type="text" class="form-control" value="<?php if(isset($promedioInst[1])) echo $promedioInst[1]?>" >
					</td>
				</tr>
				<tr>
					<td>
						<label class="col-sm-2 control-label">*Materia(3):</label>
					</td>
					<td>
						<select class="form-control" name="mat3" id="mat3" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[2])){
										if($cveMateriaInst[2]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom3" id="prom3" type="text" class="form-control" value="<?php if(isset($promedioInst[2])) echo $promedioInst[2]?>" >
					</td>
					<td>
						<label class="col-sm-2 control-label">*Materia(4):</label>
					</td>
					<td>
						<select class="form-control" name="mat4" id="mat4" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[3])){
										if($cveMateriaInst[3]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom4" id="prom4" type="text" class="form-control" value="<?php if(isset($promedioInst[3])) echo $promedioInst[3]?>" >
					</td>
				</tr>
				<tr>
					<td>
						<label class="col-sm-2 control-label">Materia(5):</label>
					</td>
					<td>
						<select class="form-control" name="mat5" id="mat5" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[4])){
										if($cveMateriaInst[4]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom5" id="prom5" type="text" class="form-control" value="<?php if(isset($promedioInst[4])) echo $promedioInst[4]?>" >
					</td>
					<td>
						<label class="col-sm-2 control-label">Materia(6):</label>
					</td>
					<td>
						<select class="form-control" name="mat6" id="mat6" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[5])){
										if($cveMateriaInst[5]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom6" id="prom6" type="text" class="form-control" value="<?php if(isset($promedioInst[5])) echo $promedioInst[5]?>" >
					</td>
				</tr>
				<tr>
					<td>
						<label class="col-sm-2 control-label">Materia(7):</label>
					</td>
					<td>
						<select class="form-control" name="mat7" id="mat7" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[6])){
										if($cveMateriaInst[6]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom7" id="prom7" type="text" class="form-control" value="<?php if(isset($promedioInst[6])) echo $promedioInst[6]?>" >
					</td>
					<td>
						<label class="col-sm-2 control-label">Materia(8):</label>
					</td>
					<td>
						<select class="form-control" name="mat8" id="mat8" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[7])){
										if($cveMateriaInst[7]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom8" id="prom8" type="text" class="form-control" value="<?php if(isset($promedioInst[7])) echo $promedioInst[7]?>" >
					</td>
				</tr>
				<tr>
					<td>
						<label class="col-sm-2 control-label">Materia(9):</label>
					</td>
					<td>
						<select class="form-control" name="mat9" id="mat9" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[8])){
										if($cveMateriaInst[8]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom9" id="prom9" type="text" class="form-control" value="<?php if(isset($promedioInst[8])) echo $promedioInst[8]?>" >
					</td>
					<td>
						<label class="col-sm-2 control-label">Materia(10):</label>
					</td>
					<td>
						<select class="form-control" name="mat10" id="mat10" onchange="validaMateria()">
								<option value="0">-Materia-</option>
							<?php
								$sql="select * from materia where status=1 order by nombre;";
								$result = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result)){
									if(isset($cveMateriaInst[9])){
										if($cveMateriaInst[9]==$row['cve_materia']){
											echo "<option value=".$row['cve_materia']." selected>".$row['nombre']."</option>";
										}else{
											echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
										}
									}else{
										echo "<option value=".$row['cve_materia'].">".$row['nombre']."</option>";
									}
								}
							?>
						</select>
					</td>
					<td> 
						<label class="col-sm-2 control-label">Promedio:</label>
					</td>
					<td>
						<input name="prom10" id="prom10" type="text" class="form-control" value="<?php if(isset($promedioInst[9])) echo $promedioInst[9];?>" >
					</td>
				</tr>
			</table>
			</br>
			<h3>Se debe seleccionar 1 hora como m&iacute;nimo</h3>
			<h5><span class="cetec">CETEC 7-piso</span> / <span class="aulas1">A1-215</span></h5>
			<table class="table table-bordered">
				<tr>
					<th>Lunes<div id="radioselect">A1</div></th>
					<th>Martes<div id="radioselect">A1</div></th>
					<th>Miercoles<div id="radioselect">A1</div></th>
					<th>Jueves<div id="radioselect">A1</div></th>
					<th>Viernes<div id="radioselect">A1</div></th>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=1
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">8:00-8:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"1\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"1\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=2
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">8:30-9:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"2\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"2\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=3
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i])
									echo "<td class=\"aulas1\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" checked></div></td>";
								else
									echo "<td class=\"cetec\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" ></div></td>";
								break;
							case 2:
								if ($donde[$i])
									echo "<td class=\"aulas1\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" checked></div></td>";
								else
									echo "<td class=\"cetec\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" ></div></td>";
								break;
							case 3:
								if ($donde[$i])
									echo "<td class=\"aulas1\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" checked></div></td>";
								else
									echo "<td class=\"cetec\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" ></div></td>";
								break;
							case 4:
								if ($donde[$i])
									echo "<td class=\"aulas1\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" checked></div></td>";
								else
									echo "<td class=\"cetec\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" ></div></td>";
								break;
							case 5:
								if ($donde[$i])
									echo "<td class=\"aulas1\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" checked></div></td>";
								else
									echo "<td class=\"cetec\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" ></div></td>";
								break;
							default:
								echo "<td class=\"none\">9:00-9:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"3\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"3\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=4
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" checked></div></td>"; 
								} else {
									echo "<td class=\"cetec\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" checked></div></td>"; 
								} else {
									echo "<td class=\"cetec\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" checked></div></td>"; 
								} else {
									echo "<td class=\"cetec\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" checked></div></td>"; 
								} else {
									echo "<td class=\"cetec\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" checked></div></td>"; 
								} else {
									echo "<td class=\"cetec\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">9:30-10:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"4\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"4\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=5
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">10:00-10:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"5\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"5\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=6
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">10:30-11:00am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"6\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"6\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=7
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\"></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">11:00-11:30am <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"7\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"7\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=8
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">11:30-12:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"8\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"8\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=9
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">12:00-12:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"9\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"9\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=10
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">12:30-1:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"10\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"10\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=11
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">1:00-1:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"11\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"11\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
					<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=12
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">2:30-3:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"12\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"12\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=13
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">3:00-3:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"13\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"13\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=14
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">3:30-4:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"14\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"14\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=15
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">4:00-4:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"15\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"15\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=16
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">4:30-5:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"16\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"16\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=17
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]){
									echo "<td class=\"aulas1\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" checked></div></td>";
								} else{
									echo "<td class=\"cetec\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">5:00-5:30pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"17\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"17\" ></div></td>";
						}
					}
				?>
				</tr>
				<tr>
				<?php
					$sql="select 
							h.dia,
							hh.hora,
							hh.ubica
						from
							horario h,
							horariohoras hh
						where
							h.cve_horario = hh.cve_horario
							and hh.hora=18
							and h.cve_usuario=$cve_usuario";
					$result = mysqli_query($con,$sql);
					for($i=0;$i<5;$i++){
						$entra[$i]=0;
						$donde[$i]=0;
					}
					$i=0;
					while($row = mysqli_fetch_array($result)){
						while($row['dia']>=$i+1){
							if($row['dia']-1==$i){
								$entra[$i]=$row['dia'];
								$donde[$i]=$row['ubica'];
							}
							$i++;
						}
					}
					for($i=0;$i<5;$i++){
						switch($entra[$i]){
							case 1:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" ></div></td>";
								}
								break;
							case 2:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" ></div></td>";
								}
								break;
							case 3:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" ></div></td>";
								}
								break;
							case 4:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" ></div></td>";
								}
								break;
							case 5:
								if ($donde[$i]) {
									echo "<td class=\"aulas1\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" checked></div></td>";
								} else {
									echo "<td class=\"cetec\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\" checked> <div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" ></div></td>";
								}
								break;
							default:
								echo "<td class=\"none\">5:30-6:00pm <input name=\"hora".$i."[]\" type=\"checkbox\" onclick=\"checkA1(this)\" value=\"18\"><div id=\"radioselect\"><input type=\"checkbox\" name=\"sede".$i."[]\" onclick=\"classA1(this)\" value=\"18\" ></div></td>";
						}
					}
				?>
				</tr>
			</table>
			<button type="submit" class="btn btn-default">Enviar</button>
		
		</center>
		</form>
		<script src="./js/validaA1.js"></script>
		<script src="./index_files/jquery-1.10.2.min.js"></script>
		<script src="./index_files/bootstrap.min.js"></script>

	</body>
</html>
<?php
	mysqli_close($con);
?>