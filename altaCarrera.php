<?php
	include "conexion.php";
	include "accesaAdmin.php";
	
	if(isset($_POST["Nombre"])){
		$nombre=$_POST["Nombre"];
		$siglas=$_POST["Siglas"];
		$sql="INSERT INTO carrera (siglas, descripcion,status) VALUES ('".$siglas."','".$nombre."', '1')";
		if (!mysqli_query($con,$sql)){
			echo "<script language=\"javascript\">
					alert(\"Error: Carrera duplicada\");
				</script>";
		}else{
			echo "<script language=\"javascript\">
						alert(\"Carrera agregada con exito!\");
					</script>";
		}
	}
	if(isset( $_GET['cve'])){
		$cve = $_GET['cve'];
		$accion = $_GET['accion'];
		if($accion == "Baja"){
			mysqli_query($con,"UPDATE carrera SET status=0 where cve_carrera=$cve") or die('Error:'. mysqli_error($con));
			echo "<script language=\"javascript\">
						alert(\"La carrera se dio de baja con exito!\");
					</script>";
		}else if($accion == "Alta"){
			mysqli_query($con,"UPDATE carrera SET status=1 where cve_carrera=$cve") or die('Error:'. mysqli_error($con));
			echo "<script language=\"javascript\">
						alert(\"La carrera se dio de alta con exito!\");
					</script>";
		}
	}	
	
	$sql="select * from carrera order by siglas;";
	$buscarPor="";
	if(isset( $_GET['buscarPor'])){
		$buscarPor=$_GET['buscarPor'];
		$buscarContenga=$_GET['buscarContenga'];
		if($buscarPor=="nombre"){
			$sql="select * from carrera where descripcion like '%$buscarContenga%' order by siglas;";
		}else if($buscarPor=="siglas"){
			$sql="select * from carrera where siglas like '%$buscarContenga%' order by siglas;";
		}else{
			$buscarContenga=strtoupper($buscarContenga);
			
			$pos = strpos($buscarContenga, "ALTA" );
			if($pos!==false)
				$sql="select * from carrera where status=1 order by siglas;";
			$pos = strpos($buscarContenga, "BAJA" );
			if($pos!==false)
				$sql="select * from carrera where status=0 order by siglas;";
		}		
	}
	
	if(!isset($_GET['exportar'])){
?>
<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Andres Cavazos">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">


		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">

		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  <a class="navbar-brand" href="indexAdmin.php">Bienvenido a MAES</a>
			</div>
			<div>
			  <ul class="nav navbar-nav" style="float:right">
				<li><a href="altaInstructor.php">Instructores</a></li>
				<li><a href="altaMateria.php">Materias</a></li>
				<li class="active"><a href="altaCarrera.php">Carreras</a></li>
				<li><a href="borrarBD.php">Borrar base de datos</a></li>
				<li><a href="ayuda.php">Ayuda</a></li>
				<li><a href="cerrarSesion.php">Cerrar Sesion</a></li>
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</div>
		
		<figure style="float:left;margin-top:-20px;position:relative">
			<img  src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>
		
		
		<center>
			<form class="form-inline" role="form" method="GET" action='altaCarrera.php'>
				<table>
					<tr>
						<td>
							<h3>Buscar por:&nbsp;</h3>
						</td>
						<td>	
							<select class="form-control" name="buscarPor">
								<option value="nombre" <?php if($buscarPor=="nombre")echo "selected";?>>Nombre</option>
								<option value="siglas"  <?php if($buscarPor=="siglas")echo "selected";?>>Siglas</option>
								<option value="estatus"  <?php if($buscarPor=="estatus")echo "selected";?>>Estatus</option>
							</select>						
						</td>
						<td>
							&nbsp;&nbsp;
						</td>
						<td>
							<h3>Que contenga:&nbsp;</h3>
						</td>
						<td>
							<input type="text" class="form-control" name="buscarContenga" <?php if(isset($buscarContenga)) echo "value=\"$buscarContenga\""?>>
						</td>
						<td>
							&nbsp;<button type="submit" class="btn btn-default">Buscar</button>
						</td>
						<td>
							&nbsp;<button type="submit" class="btn btn-default" name="exportar" value="1">Exportar</button>
						</td>
					</tr>
				</table>
			</form>
		</center>
		
		
		<div class="container">
			<table class="table table-hover" style="background-color:white;" >
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Siglas</th>
						<th>Estatus</th>
					</tr>
				</thead>
				<?php
					$result = mysqli_query($con,$sql);

					if ($result!=""){

						while($row = mysqli_fetch_array($result)){
						
							$row['status']==1 ? $status="Alta" : $status="Baja";
							if($status=="Alta"){
								echo "<tr>
											<td>".$row['descripcion']."</td>
											<td>".$row['siglas']."</td>
											<td><a href=\"altaCarrera.php?cve=".$row["cve_carrera"]."&accion=Baja\">".$status."</a></td>
										</tr>";
							}else{
								echo "<tr>
											<td>".$row['descripcion']."</td>
											<td>".$row['siglas']."</td>
											<td><a href=\"altaCarrera.php?cve=".$row["cve_carrera"]."&accion=Alta\">".$status."</a></td>
										</tr>";
							}
						}
					} else{
						echo "<tr>
									<td>---</td>
									<td>---</td>
									<td>---</td>
								</tr>";
					}
					
				?>
			</table>
		</div>
		<center>
			<form class="form-inline" role="form" method="POST" action="altaCarrera.php">
				<table>
					<tr>
						<td>
							<h3>Agregar Carrera:</h3>
						</td>
						<td>
							<label class="sr-only" for="exampleInputEmail2">Nombre</label>
							<input name="Nombre" type="text" class="form-control" id="exampleInputEmail2" placeholder="Nombre" required>
							<label class="sr-only" for="exampleInputPassword2">Siglas</label>
							<input name="Siglas" type="text" class="form-control" id="exampleInputPassword2" placeholder="Siglas"	required>
							<button type="submit" class="btn btn-default">Guardar</button>
						</td>
					</tr>
				</table>
			</form>
			<h4><a class="btn btn-lg btn-primary" href="carreraMultiple.php">Añadir m&uacute;ltiples carreras</a></h4>
		</center>
		<script src="./index_files/bootstrap.min.js"></script>

	</body>
</html>
<?php
}else{
	if($_GET['exportar']==1){

		// filename for download
		$filename = "carreras" . date('Ymd') . ".csv";

		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: text/csv; charset=UTF-16LE");

		$out = fopen("php://output", 'w');

		$flag = false;
		$result = mysqli_query($con,$sql);
		while($row = mysqli_fetch_array($result)) {
			if(!$flag) {
				// display field/column names as first row
				$titulos=array("cve_carrera","Siglas","Nombre","Estatus");
				fputcsv($out,$titulos);
				$flag = true;
			}
			//array_walk($row, 'cleanData');
			$contenido=array($row['cve_carrera'],$row['siglas'],$row['descripcion'],$row['status']);
			fputcsv($out, $contenido);
		}
		fclose($out);
	}
}	
?>
<?php
	mysqli_close($con);
?>