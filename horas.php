<?php 

function listaHorarios(){
	return Array (
			'8:00-8:30am',
			'8:30-9:00am',
			'9:00-9:30am',
			'9:30-10:00am',
			'10:00-10:30am',
			'10:30-11:00am',
			'11:30-12:00pm',
			'12:00-12:30pm',
			'12:30-1:00pm',
			'2:30-3:00pm',
			'3:00-3:30pm',
			'3:30-4:00pm',
			'4:00-4:30pm',
			'4:30-5:00pm'
		);
}

function desplegarHoras($dia){
	$arr_horas = listaHorarios();
	$cont = 1;

	foreach ($arr_horas as $value) {
		echo "<tr><td>".$value."</td><td><input name=\"hora".$dia."[]\" type=\"checkbox\" value=".$cont."></td></tr>";
		$cont++;
	}
}

/*
var_dump(listaHorarios());
echo count(listaHorarios());
*/

 ?>