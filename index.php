<?php
	include "conexion.php";
	$cve_materia=0;
	$dia=1;
	if(isset($_POST["materia"])){
		$cve_materia=$_POST["materia"];
		$dia=$_POST["dia"];
	}

	$inicio = new DateTime("2015-08-10 00:00:00");
	$hoy = new DateTime();
	$fin = new DateTime("2015-12-09 23:59:59");
?>
<!DOCTYPE html>
<!-- saved from url=(0039)http://getbootstrap.com/examples/theme/ -->
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pagina de horarios MAES">
		<meta name="author" content="Andres Cavazos">
		<link rel="icon" href="img/favicon.ico" type="image/gif">

		<title>MAES</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="css/theme.css" rel="stylesheet">

		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
		<link href="css/style.css" rel="stylesheet">
        <script>
		function submit(){
			document.getElementById("forma").submit();
		}
		</script>
	</head>

	<body role="document" style="">

		<!-- Fixed navbar -->
	    <nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="./">Bienvenido a MAES</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li class="active"><a href="iniciarSesion.html">Iniciar Sesi&oacute;n</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>

		<figure style="float:left;margin-top:-20px;position:relative">
			<img id="logoTec" src="img/logo-tecnologico.jpg" alt="Tecnologico de Monterrey">
		</figure>

		 <div class="jumbotron">
			<center>
				<img id="logoMAES" src="img/maes.jpg" alt="Tecnologico de Monterrey">
			</center>
		 </div>
		
		<div style="overflow:hidden;">
			<div id="tablahorarios" class="container theme-showcase" role="main">
				<?
					$i = date("m");
					$n = (int) $i;
					if ($hoy >= $inicio && $hoy <= $fin){
					    include 'tablaHorarios.php';
					} else if ($n >= 4 && $n <= 8) {
						echo '<h3 id="info-title">Regresamos en agosto</h3>';
					} else {
						echo '<h3 id="info-title">Regresamos en enero</h3>';
					}
				?>
			</div>
		</div>

		<script src="./index_files/jquery-1.10.2.min.js"></script>
		<script src="./index_files/bootstrap.min.js"></script>
	</body>
</html>