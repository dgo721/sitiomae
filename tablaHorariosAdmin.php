<?php
	include "accesaAdmin.php";
	include "conexion.php";
	$cve_materia=0;
	$dia=1;
	$sql="select 
				u.nombre,
				u.aPaterno,
				u.aMaterno,
				u.ubicacion,
				c.siglas,
				h.dia,
				hh.hora,
				hh.ubica,
				i.promedio
			from
				usuario u,
				imparte i,
				horario h,
				horariohoras hh,
				carrera c,
				materia m
			where
				u.cve_carrera=c.cve_carrera
				and u.cve_usuario=i.cve_usuario
				and i.cve_materia=m.cve_materia
				and h.cve_usuario=u.cve_usuario
				and h.cve_horario=hh.cve_horario
				and m.cve_materia=$cve_materia
				and h.dia=$dia";
	if(!isset($_GET['exportar'])){
		if(isset($_GET["materia"])){
			$cve_materia=$_GET["materia"];
			$dia=$_GET["dia"];
			$sql="select 
						u.nombre,
						u.aPaterno,
						u.aMaterno,
						u.ubicacion,
						c.siglas,
						h.dia,
						hh.hora,
						hh.ubica,
						i.promedio
					from
						usuario u,
						imparte i,
						horario h,
						horariohoras hh,
						carrera c,
						materia m
					where
						u.cve_carrera=c.cve_carrera
						and u.cve_usuario=i.cve_usuario
						and i.cve_materia=m.cve_materia
						and h.cve_usuario=u.cve_usuario
						and h.cve_horario=hh.cve_horario
						and m.cve_materia=$cve_materia
						and h.dia=$dia";
		}
?>

<!DOCTYPE html>

<html lang="en">
	<head>
		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<style type="text/css"></style><style id="holderjs-style" type="text/css"></style>
		<script>
		function submit()
		{
			document.getElementById("forma").submit();
		}
		</script>
	</head>
	<body role="document">
		 <div class="container theme-showcase" role="main">
				<table align="center">
					<tr>
						<td>
							<label>Buscar asesorias de:</label>
						</td>
						<td>&nbsp;</td>
						<td>
							<form action="tablaHorariosAdmin.php" method="get" name="forma">
								<select class="form-control" name="materia" onchange="submit()">
								<?php
									if($cve_materia==0)
										echo "<option selected value='0'>-Materia-</option>";
									else
										echo "<option value='0'>-Materia-</option>";
									$sql1="select * from materia where status=1 order by nombre;";
									$result = mysqli_query($con,$sql1);
									while($row = mysqli_fetch_array($result))
									{
										if($cve_materia==$row['cve_materia'])
											echo "<option value='".$row['cve_materia']."' selected>".$row['nombre']."</option>";
										else
											echo "<option value='".$row['cve_materia']."'>".$row['nombre']."</option>";
									}
								?>
								</select>
						</td>
						<td>&nbsp;</td>	
						<td>
							<label> D&iacute;a:</label>
						</td>
						<td>&nbsp;</td>	
						<td>
								<select class="form-control" name="dia" onchange="submit()">
								<?php	
									echo "<option value='1'";
									if($dia==1) 
										echo "selected";
									echo">Lunes</option>";
									echo "<option value='2'";
									if($dia==2) 
										echo "selected";
									echo">Martes</option>";
									echo "<option value='3'";
									if($dia==3) 
										echo "selected";
									echo">Miercoles</option>";
									echo "<option value='4'";
									if($dia==4) 
										echo "selected";
									echo">Jueves</option>";
									echo "<option value='5'";
									if($dia==5) 
										echo "selected";
									echo">Viernes</option>";

								?>
								</select>
						</td>
						<td>
							&nbsp;<button type="submit" class="btn btn-default" name="exportar" value="1">Exportar</button>
						</td>
						</form>
					</tr>
				</table>
			</br>
			</br>
			<table class="table table-hover" style="background-color:white;">
				<thead>
					<tr>
						<th>Instructor</th>
						<th>Carrera</th>
						<th>Promedio</th>
						<th>Disponibilidad</th>
						<th>Ubicaci&oacute;n</th>
					</tr>
				</thead>
				<?php
					
					$result = mysqli_query($con,$sql);
					$nombrePrevio="";
					$diaPrevio="";
					while($row = mysqli_fetch_array($result))
					{
						switch ($row['hora']){
							case 1:
								$hora="8:00-8:30a.m.";
								break;
							case 2:
								$hora="8:30-9:00a.m.";
								break;
							case 3:
								$hora="9:00-9:30a.m.";
								break;
							case 4:
								$hora="9:30-10:00a.m.";
								break;
							case 5:
								$hora="10:00-10:30a.m.";
								break;
							case 6:
								$hora="10:30-11:00a.m.";
								break;
							case 7:
								$hora="11:00-11:30a.m.";
								break;
							case 8:
								$hora="11:30-12:00p.m.";
								break;
							case 9:
								$hora="12:00-12:30p.m.";
								break;
							case 10:
								$hora="12:30-1:00p.m.";
								break;
							case 11:
								$hora="1:00-1:30p.m.";
								break;
							case 12:
								$hora="2:30-3:00p.m.";
								break;
							case 13:
								$hora="3:00-3:30p.m.";
								break;
							case 14:
								$hora="3:30-4:00p.m.";
								break;
							case 17:
								$hora="5:00-5:30p.m.";
								break;
							case 18:
								$hora="5:30-6:00p.m.";
								break;
						}
						$lugar = "CETEC 7-piso";
						if($row['ubica']!=0)
							$lugar = "<b>A1-215</b>";

						if($nombrePrevio==$row['nombre'].$row['aPaterno'].$row['aMaterno']){
								echo "<tr>	
										<td></td>
										<td></td>
										<td></td>
										<td>$hora</td>
										<td></td>
									</tr>";
						}else{
							echo "<tr>	
									<td>".$row['nombre']." ".$row['aPaterno']." ".$row['aMaterno']."</td>
									<td>".$row['siglas']."</td>
									<td>".$row['promedio']."</td>
									<td>$hora</td>
									<td>".$lugar."</td>
								</tr>";
							$nombrePrevio=$row['nombre'].$row['aPaterno'].$row['aMaterno'];
						}
					}
				?>
			</table>
		</div>
		<script src="./index_files/bootstrap.min.js"></script>
	</body>
</html>
<?php
}else{
	if($_GET['exportar']==1){
	////////////////////////////////////////////////////////////////////////////////REVISAR////////////////////////////////////////////////////////////////////////////////////////////
		$sql="select 
				m.nombre,
				h.dia,
				hh.hora
			from 
				imparte i,
				materia m,
				horario h,
				horariohoras hh
			where
				i.cve_materia=m.cve_materia
				and i.cve_usuario=h.cve_usuario
				and h.cve_horario=hh.cve_horario
				and m.status=1
			group by hora,dia,nombre
			order by nombre,dia,hora";
		
		$nombreMateriaAnterior=""; //Para saber cuando reiniciar los arreglos
		for($i=1;$i<=16;$i++){ //For que inicializa los arreglos de las horas por dia
			$lunes[$i]="-";
			$martes[$i]="-";
			$miercoles[$i]="-";
			$jueves[$i]="-";
			$viernes[$i]="-";
		}
		
		$diaAcual="";//Variable para verificar el cambio de dia
		$primeraIteracion=1;
		
		// filename for download
		$filename = "Horarios" . date('Ymd') . ".csv";

		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: text/csv; charset=UTF-16LE");
	
		$out = fopen("php://output", 'w');
		$flag = true;
		$result = mysqli_query($con,$sql);
		
		while($row = mysqli_fetch_array($result)) {
		
			$nombreMateria=$row['nombre'];
			if($nombreMateriaAnterior!=$nombreMateria && $nombreMateriaAnterior!=""){
				for($i=1;$i<=16;$i++){
					$contenido=array($nombreMateriaAnterior,$lunes[$i],$martes[$i],$miercoles[$i],$jueves[$i],$viernes[$i]);
					fputcsv($out, $contenido);
				}
				
				$contenido=array("","","","","","");
				fputcsv($out, $contenido);
				
				for($i=1;$i<=16;$i++){
					$lunes[$i]="-";
					$martes[$i]="-";
					$miercoles[$i]="-";
					$jueves[$i]="-";
					$viernes[$i]="-";
				}
				$nombreMateriaAnterior=$nombreMateria;
			}
			
			$diaActual=$row['dia'];
			
			if($flag) {
				// display field/column names as first row
				$titulos=array("Materia","Lunes","Martes","Miercoles","Jueves","Viernes");
				fputcsv($out,$titulos);
				$flag = false;
			}
			switch ($row['hora']){
				case 1:
					$hora="8:00-8:30a.m.";
					break;
				case 2:
					$hora="8:30-9:00a.m.";
					break;
				case 3:
					$hora="9:00-9:30a.m.";
					break;
				case 4:
					$hora="9:30-10:00a.m.";
					break;
				case 5:
					$hora="10:00-10:30a.m.";
					break;
				case 6:
					$hora="10:30-11:00a.m.";
					break;
				case 7:
					$hora="11:00-11:30a.m.";
					break;
				case 8:
					$hora="11:30-12:00p.m.";
					break;
				case 9:
					$hora="12:00-12:30p.m.";
					break;
				case 10:
					$hora="12:30-1:00p.m.";
					break;
				case 11:
					$hora="1:00-1:30p.m.";
					break;
				case 12:
					$hora="2:30-3:00p.m.";
					break;
				case 13:
					$hora="3:00-3:30p.m.";
					break;
				case 14:
					$hora="3:30-4:00p.m.";
					break;
				case 15:
					$hora="4:00-4:30p.m.";
					break;
				case 16:
					$hora="4:30-5:00p.m.";
					break;
			}
			
			switch($diaActual){
				case 1:
					$lunes[$row['hora']]=$hora;
					break;
				case 2:
					$martes[$row['hora']]=$hora;
					break;
				case 3:
					$miercoles[$row['hora']]=$hora;
					break;
				case 4:
					$jueves[$row['hora']]=$hora;
					break;
				case 5:
					$viernes[$row['hora']]=$hora;
					break;
			}
			
			if($primeraIteracion==1)
			{
				$nombreMateriaAnterior=$nombreMateria;
				$primeraIteracion=0;
			}
		}
		for($i=1;$i<=16;$i++){
			$contenido=array($nombreMateriaAnterior,$lunes[$i],$martes[$i],$miercoles[$i],$jueves[$i],$viernes[$i]);
			fputcsv($out, $contenido);
		}
		fclose($out);
	}
}

mysqli_close($con);
?>
	